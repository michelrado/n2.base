namespace N2.Base {
	public static class Defaults {
		public static class Containers {
			public const string Metadata = "Metadata";
			public const string Content = "Content";
			public const string SEO = "SEO";			
			public const string Advanced = "Advanced";
			public const string Settings = "Settings";
			public const string MailSettings = "MailSettings";
			public const string Footer = "Footer";
			public const string HeaderArea = "HeaderArea";
			public const string SiteArea = "RootSettings";
			public const string Teasers = "Teasers";
		}

		public static class Zones {
			public const string RightTeasers = "RightTeasers";
		}
	}
}