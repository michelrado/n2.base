using System.Web.Mvc;
using System.Web.Routing;

using N2.Base.Controllers;
using N2.Definitions.Runtime;
using N2.Engine;
using N2.Plugin;
using N2.Web.Mvc;

namespace N2.Base {
	[AutoInitialize]
	public class N2CmsInitializer : IPluginInitializer {
		public void Initialize(IEngine engine) {
			RegisterControllerFactory(ControllerBuilder.Current, engine);
			RegisterRoutes(RouteTable.Routes, engine);
			RegisterViewEngines(ViewEngines.Engines);
			RegisterViewTemplates(engine);
		}

		private void RegisterViewTemplates(IEngine engine) {
			engine.RegisterViewTemplates<PageController>().Add<PartController>();
		}

		public static void RegisterControllerFactory(ControllerBuilder controllerBuilder, IEngine engine) {
			engine.RegisterAllControllers();
			var controllerFactory = engine.Resolve<ControllerFactoryConfigurator>()
				.NotFound<StartPageController>(sc => sc.NotFound())
				.ControllerFactory;
			controllerBuilder.SetControllerFactory(controllerFactory);
		}

		public static void RegisterRoutes(RouteCollection routes, IEngine engine) {			
			routes.MapContentRoute("Content", engine);
		}

		public static void RegisterViewEngines(ViewEngineCollection viewEngines) {
			viewEngines.DecorateViewTemplateRegistration();
			viewEngines.Add(new CustomViewEngine());
		}
	}
}