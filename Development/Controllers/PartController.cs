using System.Web.Mvc;

using N2.Base.Models.Parts;
using N2.Web;
using N2.Web.Mvc;

namespace N2.Base.Controllers {
	[Controls(typeof(PartModelBase))]
	public class PartController : ContentController<PartModelBase> {
		public override ActionResult Index() {
			return PartialView(CurrentItem.GetContentType().Name, CurrentItem);
		}
	}
}