using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using N2.Base.Models.Pages;
using N2.Engine.Globalization;
using N2.Web.Mvc;

namespace N2.Base.Controllers {
	public class PageControllerBase<T> : ContentController<T> where T : PageBase {
		private readonly LanguageGatewaySelector languageGateway;

		public PageControllerBase(LanguageGatewaySelector languageGateway) {
			this.languageGateway = languageGateway;
		}

		protected void SetLanguages() {
			CurrentItem.LanguageVersions = GetLanguages();
		}

		public override ActionResult Index() {
			SetLanguages();
			return View(CurrentItem.GetContentType().Name, CurrentItem);
		}

		private IEnumerable<ILanguageItem> GetLanguages() {
			var gateway = this.languageGateway.GetLanguageGateway(CurrentItem);
			var translations = gateway.FindTranslations(CurrentItem);

			foreach(var translatedPage in translations) {
				if(CurrentItem != translatedPage) {
					yield return new LanguageItem {
						Language = gateway.GetLanguage(translatedPage),
						Url = translatedPage.Url,
						LanguageText = GetStartPageForPage(translatedPage).LanguageLinkText
					};
				}
			}
		}

		private StartPage GetStartPageForPage(ContentItem page) {
			if(page is StartPage) {
				return page as StartPage;
			}
			return Find.EnumerateParents(page).First(p => p is StartPage) as StartPage;
		}
	}
}