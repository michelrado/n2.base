using N2.Engine.Globalization;

namespace N2.Base.Controllers {
	public class LanguageItem : ILanguageItem {
		public ILanguage Language { get; set; }
		public string Url { get; set; }
		public string LanguageText { get; set; }
	}
}