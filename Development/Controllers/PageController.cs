using N2.Base.Models.Pages;
using N2.Engine.Globalization;
using N2.Web;

namespace N2.Base.Controllers {
	[Controls(typeof(PageBase))]
	public class PageController : PageControllerBase<PageBase> {
		
		public PageController(LanguageGatewaySelector languageGateway) : base(languageGateway) {}
	}
}