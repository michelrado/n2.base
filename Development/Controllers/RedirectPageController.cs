using System.Web.Mvc;

using N2.Base.Models.Pages;
using N2.Engine.Globalization;
using N2.Web;

namespace N2.Base.Controllers {
    [Controls(typeof(RedirectPage))]
    public class RedirectPageController : PageControllerBase<RedirectPage> {
	    public RedirectPageController(LanguageGatewaySelector languageGateway) : base(languageGateway) {}

	    public override ActionResult Index() {
            var item = CurrentItem;
			SetLanguages();
            item.IsEditor = (HttpContext.User.Identity.IsAuthenticated && Content.Engine.SecurityManager.IsEditor(HttpContext.User));
            return View(CurrentItem.GetContentType().Name, item);
        }
    }
}