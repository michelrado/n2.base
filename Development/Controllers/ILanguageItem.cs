using N2.Engine.Globalization;

namespace N2.Base.Controllers {
	public interface ILanguageItem {
		ILanguage Language { get; set; }
		string Url { get; set; }
		string LanguageText { get; set; }
	}
}