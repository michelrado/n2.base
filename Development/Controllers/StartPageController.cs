using System.Web.Mvc;

using N2.Base.Models.Pages;
using N2.Engine.Globalization;
using N2.Web;

namespace N2.Base.Controllers {
	[Controls(typeof(StartPage))]
	public class StartPageController : PageControllerBase<StartPage> {
		public StartPageController(LanguageGatewaySelector languageGateway) : base(languageGateway) {}

		public ActionResult NotFound() {
			Response.TrySkipIisCustomErrors = true;
			Response.Status = "404 Not Found!";
			return View();
		}
	}
}