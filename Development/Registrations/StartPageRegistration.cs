using N2.Base.Controllers;
using N2.Base.Models.Pages;
using N2.Definitions.Runtime;
using N2.Installation;
using N2.Web.Mvc;

namespace N2.Base.Registrations {
	[Registration]
	public class StartPageRegisterer : FluentRegisterer<StartPage> {
		public override void RegisterDefinition(IContentRegistration<StartPage> register) {
			register.ControlledBy<StartPageController>();
			register.Page(title: "Startsida", description: "Startsida f�r en spr�kversion an webbplatsen.");
			register.IconClass("n2-icon-globe");
			register.Definition.Installer = InstallerHint.PreferredStartPage;
			register.RestrictParents(typeof(RootPage), typeof(StartPage));
			register.On(c => c.Text)
				.FreeText("Text")
				.Container(Defaults.Containers.HeaderArea)
				.SortOrder(110);
		}
	}
}