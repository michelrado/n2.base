using N2.Base.Models.Pages;
using N2.Definitions;
using N2.Definitions.Runtime;

namespace N2.Base.Registrations {
	[Registration]
	public class RedirectPageRegisterer : FluentRegisterer<RedirectPage> {
		public override void RegisterDefinition(IContentRegistration<RedirectPage> register) {
			register.Page(title: "Omdirigeringssida", description: "En sida f�r att omdirigera trafik.");
			register.IconClass("n2-icon-link");
			register.RestrictParents(typeof(IStructuralPage));

			register.On(c => c.Destination)
				.Text("Redirect Url");
		}
	}
}