﻿using System.Linq;
using System.Web.Mvc;

namespace N2.Base {
	public class CustomViewEngine : RazorViewEngine {
		private static string[] partialViewFormats = {
			"~/Views/Parts/{0}.cshtml"
		};

		private static string[] viewFormats = {
			"~/Views/Pages/{0}.cshtml"
		};

		public CustomViewEngine() {
			PartialViewLocationFormats = PartialViewLocationFormats.Union(partialViewFormats).ToArray();
			ViewLocationFormats = ViewLocationFormats.Union(viewFormats).ToArray();
		}
	}
}