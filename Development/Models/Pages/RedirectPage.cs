namespace N2.Base.Models.Pages {
	public class RedirectPage : PageBase {
		public virtual string Destination { get; set; }
		public virtual bool IsEditor { get; set; }
	}
}