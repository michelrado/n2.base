﻿using System.Web.UI.WebControls;

using N2.Details;
using N2.Web.UI;

namespace N2.Base.Models.Pages {
	[FieldSetContainer(Defaults.Containers.HeaderArea, "Header", 20, ContainerName = Defaults.Containers.Content)]
	public class HeaderBasePage : PageBase {
		[EditableTextBox(TextMode = TextBoxMode.SingleLine, SortOrder = 100, ContainerName = Defaults.Containers.HeaderArea)]
		public virtual string Header {
			get {
				var header = GetDetail("Header", "");
				return string.IsNullOrEmpty(header) ? Title : header;
			}
			set { SetDetail("Header", value); }
		}
	}
}