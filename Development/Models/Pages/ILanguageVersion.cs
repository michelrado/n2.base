using System.Collections.Generic;

using N2.Base.Controllers;

namespace N2.Base.Models.Pages {
	public interface ILanguageVersion {
		IEnumerable<ILanguageItem> LanguageVersions { get; set; }
	}
}