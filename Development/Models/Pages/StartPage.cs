using System.Globalization;
using System.Web.UI.WebControls;

using N2.Definitions;
using N2.Details;
using N2.Engine.Globalization;
using N2.Integrity;

namespace N2.Base.Models.Pages {
	[RestrictParents(typeof(IRootPage), typeof(IStartPage))]
	public class StartPage : HeaderBasePage, IStartPage, ILanguage, IStructuralPage {
		[EditableLanguagesDropDown(Title = "Language", ContainerName = Defaults.Containers.Advanced, SortOrder = 200)]
		public virtual string LanguageCode { get; set; }		

		[EditableTextBox("Switch language text", 110, TextMode = TextBoxMode.SingleLine, ContainerName = Defaults.Containers.Advanced)]
		public virtual string LanguageLinkText { get; set; }

		public string LanguageTitle {
			get {
				if(string.IsNullOrEmpty(LanguageCode)) {
					return "";
				}
				return new CultureInfo(LanguageCode).DisplayName;
			}
		}

		public virtual string Text { get; set; }
	}
}