﻿using System.Web.UI.WebControls;

using N2.Details;
using N2.Installation;
using N2.Integrity;
using N2.Management.Myself;
using N2.Security;
using N2.Web.UI;

namespace N2.Base.Models.Pages {
	[PageDefinition("Root Page",
		Description = "A root page used to organize start pages.",
		SortOrder = 0,
		InstallerVisibility = InstallerHint.PreferredRootPage,
		IconClass = "n2-icon-sun",
		TemplateUrl = "{ManagementUrl}/Myself/Root.aspx")]
	[RestrictParents(AllowedTypes.None)]
	[RecursiveContainer(Defaults.Containers.SiteArea, 70, RequiredPermission = Permission.Administer)]
	[TabContainer(Defaults.Containers.MailSettings, "Mail settings", 3, ContainerName = Defaults.Containers.SiteArea)]
	[TabContainer(Defaults.Containers.Settings, "Settings", 5, ContainerName = Defaults.Containers.SiteArea)]
	[WithEditableTitle]
	public class RootPage : RootBase {
		[EditableTextBox("Title suffix", 10, TextMode = TextBoxMode.SingleLine,
			ContainerName = Defaults.Containers.Settings)]
		public virtual string TitleSuffix {
			get { return (string)GetDetail("TitleSuffix") ?? ""; }
			set { SetDetail("TitleSuffix", value); }
		}

		[EditableTextBox("Google Analytics", 15, TextMode = TextBoxMode.SingleLine,
			ContainerName = Defaults.Containers.Settings)]
		public virtual string GoogleAnalyticsCode { get; set; }

		[EditableTextBox("SMTP server", 5, TextMode = TextBoxMode.SingleLine,
			ContainerName = Defaults.Containers.MailSettings)]
		public virtual string SMTPServer { get; set; }

		[EditableTextBox("To mail address", 10, TextMode = TextBoxMode.SingleLine,
			ContainerName = Defaults.Containers.MailSettings)]
		public virtual string ToMailAddress { get; set; }
	}
}