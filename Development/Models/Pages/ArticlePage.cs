﻿using System.Collections.Generic;
using System.Web.UI.WebControls;

using N2.Details;
using N2.Integrity;

namespace N2.Base.Models.Pages {
	[PageDefinition("Artikelsida",
		Description = "En sida för artiklar.",
		SortOrder = 800)]
	[RestrictParents(typeof(StartPage), typeof(ArticlePage))]
	[AvailableZone("Right", Defaults.Zones.RightTeasers)]
	public class ArticlePage : HeaderBasePage {
		[EditableTextBox("Preamble", 110, TextMode = TextBoxMode.MultiLine, Rows = 10,
			ContainerName = Defaults.Containers.HeaderArea)]
		public virtual string Preamble { get; set; }

		[EditableFreeTextArea("Text", 120, ContainerName = Defaults.Containers.HeaderArea)]
		public virtual string Text { get; set; }

		[EditableChildren("Right teasers", Defaults.Zones.RightTeasers, 200, HelpText = "",
			ContainerName = Defaults.Containers.Teasers)]
		public virtual IEnumerable<ContentItem> RightTeasers { get; set; }
	}
}