using System.Collections.Generic;
using System.Web.UI.WebControls;

using N2.Base.Controllers;
using N2.Collections;
using N2.Definitions;
using N2.Details;
using N2.Integrity;
using N2.Web.UI;

namespace N2.Base.Models.Pages {
	[WithEditableTitle]
	[WithEditableName(ContainerName = Defaults.Containers.Metadata)]
	[WithEditableVisibility(ContainerName = Defaults.Containers.Metadata)]
	[WithEditablePublishedRange("Published Between", 20, ContainerName = Defaults.Containers.Advanced,
		BetweenText = " and ")]
	[SidebarContainer(Defaults.Containers.Metadata, 100, HeadingText = "Metadata")]
	[TabContainer(Defaults.Containers.Content, "Content", 10)]
	[TabContainer(Defaults.Containers.SEO, "SEO", 20)]
	[TabContainer(Defaults.Containers.Advanced, "Advanced", 15)]
	[TabContainer(Defaults.Containers.Teasers, "Teasers", 30)]
	[TabContainer(Defaults.Containers.Footer, "Footer", 40)]
	[RestrictParents(typeof(IPage))]
	public abstract class PageBase : ContentItem, IPage, ILanguageVersion {
		[EditableTextBox(TextMode = TextBoxMode.SingleLine, ContainerName = Defaults.Containers.SEO, SortOrder = 10)]
		public virtual string SeoTitle {
			get {
				var seoTitle = GetDetail("SeoTitle", "");
				return string.Format("{0}{1}",
				                     (string.IsNullOrEmpty(seoTitle) ? Title : seoTitle),
				                     SiteFind.RootItem.TitleSuffix);
			}
			set { SetDetail("SeoTitle", value); }
		}

		[EditableTextBox(TextMode = TextBoxMode.MultiLine, Rows = 10, ContainerName = Defaults.Containers.SEO, SortOrder = 20)]
		public virtual string Description { get; set; }

		public virtual IList<T> GetChildren<T>() where T : ContentItem {
			return new ItemList<T>(Children, new AccessFilter(), new TypeFilter(typeof(T)));
		}

		public virtual IList<T> GetChildren<T>(string zoneName) where T : ContentItem {
			return new ItemList<T>(Children, new AccessFilter(), new TypeFilter(typeof(T)), new ZoneFilter(zoneName));
		}

		public IEnumerable<ILanguageItem> LanguageVersions { get; set; }
	}
}