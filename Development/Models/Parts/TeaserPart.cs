﻿using System.Web.UI.WebControls;

using N2.Details;
using N2.Integrity;

namespace N2.Base.Models.Parts {
	[PartDefinition("Teaser"),
	 AllowedZones(new[] { Defaults.Zones.RightTeasers })]
	public class TeaserPart : PartModelBase {
		[EditableText("Heading", 10)]
		public override string Title { get; set; }

		[EditableImage("Image", 15)]
		public virtual string ImageUrl { get; set; }

		[EditableTextBox("Preamble", 20, TextMode = TextBoxMode.MultiLine, Rows = 10)]
		public virtual string Preamble { get; set; }

		[EditableUrl("LinkUrl", 30)]
		public virtual string LinkUrl { get; set; }
	}
}