using N2.Base.Models.Pages;
using N2.Definitions;
using N2.Web.UI;

namespace N2.Base.Models.Parts {
	[SidebarContainer(Defaults.Containers.Metadata, 100, HeadingText = "Metadata")]
	public abstract class PartModelBase : ContentItem, IPart {}
}