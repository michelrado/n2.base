﻿using N2.Base.Models.Pages;
using N2.Persistence;

namespace N2.Base
{
	public class SiteFind : GenericFind<RootPage, StartPage> {}
}