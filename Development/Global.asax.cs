﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace N2.Base {
	public class Global : HttpApplication {
		public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
			filters.Add(new HandleErrorAttribute());
		}

		public static void RegisterRoutes(RouteCollection routes) {
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
		}

		public static void RegisterBundles(BundleCollection bundles) {
			bundles.UseCdn = true;
			const string jqueryCdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.0.min.js";
			bundles.Add(new ScriptBundle("~/bundles/jquery", jqueryCdnPath).Include("~/Content/Scripts/Lib/jquery-{version}.js"));
			bundles.Add(new ScriptBundle("~/Content/Scripts/bundle").Include("~/Content/Scripts/script.js"));
			bundles.Add(new StyleBundle("~/Content/Styles/bundle").Include("~/Content/Styles/style.css"));
		}

		protected void Application_Start() {
			AreaRegistration.RegisterAllAreas();
			RegisterGlobalFilters(GlobalFilters.Filters);
			RegisterRoutes(RouteTable.Routes);
			RegisterBundles(BundleTable.Bundles);
		}
	}
}